package week3.Task1.domain;

public class Shape {
    private Color color;
    private boolean filled;

    public Shape(){
        setColor(Color.GREEN);
        filled=true;
    }
    public Shape(Color color,boolean filled ){
        setColor(color);
        setFilled(filled);
    }
    public boolean isFilled(){
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    @Override
    public String toString() {
        return " A Shape with color "+color+" and "+(filled?" Filled":" Not Filled");
    }
}
