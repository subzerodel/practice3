package week3.Task1.domain;

public class Circle extends Shape {
    private double radius;

    public Circle() {
        radius=1.0;

    }
    public Circle(Color color,boolean filled,double radius) {
        super(color,filled);
        setRadius(radius);
    }
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI* Math.pow(radius, 2);
    }

    public double getPerimeter() {
        return 3.14 * 2 * radius;
    }

    @Override
    public String toString() {
        return "A circle with radius: "+radius+",which is subclass of "+super.toString();
    }
}
