package week3.Task1.domain;

public enum Color {
    RED("red"),
    GREEN("green"),
    PINK("pink"),
    BLACK("black"),
    WHITE("white");

    private String color;

    Color(String color){
        setColor(color);
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return color;
    }
}

