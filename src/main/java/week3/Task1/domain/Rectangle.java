package week3.Task1.domain;

public class Rectangle extends Shape{
    private double width;
    private double length;

    public Rectangle(){
        width=1.0;
        length=1.0;
    }

    public Rectangle(double width, double length){
        setWidth(width);
        setLength(length);
    }

    public Rectangle(Color color,boolean filled, double width, double length){
        super(color,filled);
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return width*length;
    }

    public double getPerimeter(){
        return (width+length)*2;
    }


    @Override
    public String toString() {
        return "A rectangle  with width: "+width+" length "+length+",which is subclass of "+super.toString();
    }
}
