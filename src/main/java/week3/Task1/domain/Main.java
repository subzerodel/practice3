package week3.Task1.domain;

public class Main {
    public static void main(String[] args){
        Shape circle= new Circle(Color.RED,true,2.0);
        Shape rectangle= new Rectangle(Color.RED,true,5.0,6.0);
        Shape square= new Square(Color.PINK,false,8);

        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println(square);
    }
}
